export enum CodeAddress {
  BE,
  EXT
}
export interface Cotisation {
  personId: number;
  fromDate: Date;
  endDate: Date;
  status: 'Payer' | 'Non Payer';
}

export interface Affiliate {
  personId: number;
  federal: string;
  nom: string;
  prenom: string;
  addresses: Array<Address>;
  niss: string;
  cotisations: Array<Cotisation>;
}

export interface Address {
  addressId: number;
  street: string;
  num: string;
  cp: string;
  city: string;
  codeAddress: CodeAddress;
}
