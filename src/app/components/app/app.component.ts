import {AfterViewInit, Component, ElementRef, OnChanges, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Affiliate} from '../../models/mutuel';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit, OnChanges {
  title = 'FMSBFormation';
  nom: string;
  color: string = '#000';

  @ViewChild('divElement', {static: false}) el: ElementRef<HTMLDivElement>;

  ngOnInit(): void {
    console.log('OnInit');
  }

  ngAfterViewInit(): void {
    console.log(`View Init`);
    console.log(this.el.nativeElement);
  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }


  sayHello(msg: string) {
    console.log(msg);
  }
}
